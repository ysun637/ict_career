//make an array to storage the data we will use for table
var tableData = [{
    Role: "Software_Developer_&_Programmer",
    Job_Vacancies_Aug_2018: 590,
    Salary_Max: 100,
    Salary_Min: 72,
    Skills_and_Knowledge: [
        "computer software and systems",
        "programming languages and techniques",
        "software development processes such as Agile",
        "confidentiality, data security and data protection issues"
    ],
    Job_Description: "Software developers and programmers develop and maintain computer software, websites and software applications (apps)",
    image: "pic1.jpg"
},

    {
        Role: "Database_&_Systems_Administration",
        Job_Vacancies_Aug_2018: 74,
        Salary_Max: 90,
        Salary_Min: 66,
        Skills_and_Knowledge: [
            "a range of database technologies and operating systems",
            "new developments in databases and security systems",
            "computer and database principles and protocols"],
        Job_Description: "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        image: "pic2.jpg"
    },

    {
        Role: "Help_Desk_&_IT_Support",
        Job_Vacancies_Aug_2018: 143,
        Salary_Max: 65,
        Salary_Min: 46,
        Skills_and_Knowledge: [
            "computer hardware, software, networks and websites",
            "the latest developments in information technology"
        ],
        Job_Description: "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        image: "pic3.jpg"
    },

    {
        Role: "Data_Analyst",
        Job_Vacancies_Aug_2018: 270,
        Salary_Max: 128,
        Salary_Min: 69,
        Skills_and_Knowledge: [
            "data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R",
            "data analysis, mapping and modelling techniques",
            "analytical techniques such as data mining"
        ],
        Job_Description: "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        image: "pic4.jpg"
    },

    {
        Role: "Test_Analyst",
        Job_Vacancies_Aug_2018: 127,
        Salary_Max: 98,
        Salary_Min: 70,
        Skills_and_Knowledge: [
            "programming methods and technology",
            "computer software and systems",
            "project management"
        ],
        Job_Description: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        image: "pic5.jpg"
    },

    {
        Role: "Project_Management",
        Job_Vacancies_Aug_2018: 188,
        Salary_Max: 190,
        Salary_Min: 110,
        Skills_and_Knowledge: [
            "principles of project management",
            "approaches and techniques such as Kanban and continuous testing",
            "how to handle software development issues",
            "common web technologies used by the scrum team"
        ],
        Job_Description: "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        image: "pic6.jpg"
    },
    {       //make last line empty for storage the total value and average value
          Role: " ",
        Job_Vacancies_Aug_2018: " ",
        Salary_Max: " ",
        Salary_Min: " "
    }
];





//to create the table, use loop to add information from array we made before to the table
$(document).ready(function () {

    for (var i = 0; i < tableData.length; i++) {
        if (i < tableData.length - 1) { //create all the lines of the table except last line.
            $(".tabledata").append("<tr>"
                + "<td>" + tableData[i].Role + "</td>"
                + "<td>" + tableData[i].Job_Vacancies_Aug_2018 + "</td>"
                + "<td>" + tableData[i].Salary_Max + "</td>"
                + "<td>" + tableData[i].Salary_Min + "</td>"
                + "</tr>");
            // last line of the table
        } else {
            // alert("=====else");
            $(".tabledata").append("<tr>"
                + "<td>" + tableData[i].Role + "</td>"
                + "<td>" + getTotal() + "</td>"
                + "<td>" + getMaxAvg() + "</td>"
                + "<td>" + getMinAvg() + "</td>"
                + "</tr>");
            // alert("=======else======end");
        }
    }

});
// get the total value of jobs Vacancies of August of 2018
function getTotal() {
    // alert(1111111);
    var a = 0;

    for (var i = 0; i < tableData.length - 1; i++) {
        a += tableData[i].Job_Vacancies_Aug_2018;
    }
    // alert(22222);
    return "Total: " + a;
}

//get the Average salary of Maximum salary
function getMaxAvg() {
    // alert("=======max========");
    var n = 0;

    for (var i = 0; i < tableData.length - 1; i++) {
        n += tableData[i].Salary_Max;
    }
    // alert("=======max   for========");
    var b = Math.round(n / (tableData.length - 1));
    // alert("=======max========end");
    return "Avg: " + b;
}

//get the Average salary of Minimum salary
function getMinAvg() {
    // alert("=======min========");
    var n = 0;
    // var c = 0;
    for (var i = 0; i < tableData.length - 1; i++) {
        n += tableData[i].Salary_Min;
    }
    var c = Math.round(n / (tableData.length - 1));
    // alert("=======in========end");
    return "Avg: " + c;
}

//when user click on different lines of the table ,then change to the information which related to that line.
     $(document).on("click", "tr", function () {//tr represent to the entire line, these code means when user click on a line of the table.
         var role = this.firstChild.textContent;//declear a variable which stands for the first  words of the table
         for (var i = 0; i < tableData.length-1; i++) {
             if (role == tableData[i].Role) {//if user click on the line of the table which line's first words equals to i element's role
                 $(".changeTableInfo")[0].src = tableData[i].image;//change which class equals to changeTableInfo to the picture to i picture
                 $("span")[1].textContent = tableData[i].Role;//change the text to i's text(same explanation )
                 $(".changeTableInfo")[2].textContent = tableData[i].Job_Description;// change to i's Job_Description
                 $("li").hide();
                 for (var i0 = 0; i0 <tableData[i].Skills_and_Knowledge.length; i0++) { //change to i's Skills_and_Knowledge
                     var require = $("ul").append("<li>" + tableData[i].Skills_and_Knowledge[i0] + "</li>");
                 }
             }
         }

});





